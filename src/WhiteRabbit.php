<?php

class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile($filePath)
    {
        return preg_replace('/[^a-z]/', '', strtolower(file_get_contents($filePath)));
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {
        $strArr = str_split($parsedFile);
        $newArr = [];
        for ($i = 0; $i < strlen($parsedFile); $i++) {
            $newArr[$strArr[$i]] += 1;
        }
        arsort($newArr);
        $keys = array_keys($newArr);
        $index = floor(count($keys) / 2);
        $letter = $keys[$index];
        $occurrences = $newArr[$keys[$index]];
        return $letter;
    }
}