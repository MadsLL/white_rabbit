<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */

    public function findCashPayment($amount){
        $arr = array(
            '1'   => 0,
            '2'   => 0,
            '5'   => 0,
            '10'  => 0,
            '20'  => 0,
            '50'  => 0,
            '100' => 0
        );

        $coins = array(100, 50, 20, 10, 5, 2, 1);

        $thisDone = False;

        while($thisDone == False) {
            if ($amount <= 0) {
                $thisDone = True;
            }

            for ($i = 0; $i < sizeof($coins); $i++) {
                if ($amount >= $coins[$i]) {
                    $arr[$coins[$i]] += 1;
                    $amount -= $coins[$i];
                    break;
                }
            }
        }

        return $arr;
    }
}